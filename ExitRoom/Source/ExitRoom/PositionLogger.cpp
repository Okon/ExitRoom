// Fill out your copyright notice in the Description page of Project Settings.

#include "PositionLogger.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UPositionLogger::UPositionLogger()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void UPositionLogger::BeginPlay()
{
	Super::BeginPlay();

	FString name = GetOwner() -> GetName();
	FString globalPosition = GetOwner()->GetActorTransform().GetLocation().ToString();

	// * here is overloaded operator
	UE_LOG(LogTemp, Error, TEXT("This is %s, in the location of %s"), *name, *globalPosition);

}


// Called every frame
void UPositionLogger::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

